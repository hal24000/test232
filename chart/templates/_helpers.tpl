{{- define "apilake.name" -}}
{{- ternary .Release.Name (printf "%s-%s" .Release.Name .Chart.Name ) (contains .Chart.Name .Release.Name) -}}
{{- end -}}

{{- define "apilake.labels" -}}
helm.sh/chart: {{ printf "%s-%s" .Chart.Name .Chart.Version }}
app.kubernetes.io/name: {{ printf "%s" .Chart.Name }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Chart.Version }}
{{- end -}}

{{- define "apilake.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "apilake.urlPrefix" -}}
{{- printf "/stui/%s/v%d/" (include "apilake.name" .) (int64 .Values.devOps.majorVer) }}
{{- end -}}

{{- define "apilake.resources" -}}
limits:
  cpu: {{.Values.resources.limits.cpu}}
  memory: {{.Values.resources.limits.memory}}
requests:
  cpu: {{.Values.resources.requests.cpu}}
  memory: {{.Values.resources.requests.memory}}
{{- end -}}

